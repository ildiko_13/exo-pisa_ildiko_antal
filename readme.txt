In a renaissance tower you can group windows as long as the weight from the floors above allows it.
For instance, in many towers and bell towers of the period you have the topmost floors with windows
grouped by three or more and then the floors below with ever smaller groups of windows :

see https://en.wikipedia.org/wiki/San_Donato,_Genoa#/media/File:Tower_-_Church_of_San_Donato_-_Genoa_2014_(2).JPG

The goal is building such a tower and respecting this rule.