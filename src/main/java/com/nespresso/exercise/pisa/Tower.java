package com.nespresso.exercise.pisa;

import java.util.ArrayList;
import java.util.List;

public class Tower {

    List<Floor> floors = new ArrayList<Floor>();

    public void addFloor(int floorSize) {
        Floor floor = new Floor(floorSize);
        this.addFloor(floor);
//        this.reArrangeLowerFloors(floors.size() - 1);
    }

    public String printFloor(int floorIndex) {
        if (floorIndex >= floors.size()) {
            throw new UnsupportedOperationException("Floor doesn't exists");
        }
        Floor floor = floors.get(floorIndex);
        return floor.printOut();

    }

    public void addFloorWithWindows(int floorSize, int desiredNumberOfWindows) {
        Floor floor = new Floor(floorSize, desiredNumberOfWindows);
        this.addFloor(floor);
    }

    private void addFloor(Floor floor) {
        int floorsCount = floors.size() - 1;
        if (floorsCount >= 0) {
            Floor previousFloor = floors.get(floorsCount);
            if (previousFloor.printOut().length() < floor.getFloorSize()) {
                throw new IllegalArgumentException("The previous floor is smaller");
            }

        }
        floors.add(floor);
    }

    private void reArrangeLowerFloors(int level) {
        if (level < 0) {
            return;
        } else {
            int nextFloorsNumber = floors.size() - level;
            //1 floor => groups max 3 window below
            //2 floor => groups max 2 window below
            //2 floor => groups max 1 window below
            switchWindowWithWalls(nextFloorsNumber, floors.get(level));
            reArrangeLowerFloors(level - 1);
        }
    }

    private void switchWindowWithWalls(int groupsMaxNumber, Floor floor) {
        // change the layout of the current floor

        StringBuilder sb = new StringBuilder(floor.getLayout());
        int windowsNumber = 0;
        for (int i = 1; i < floor.getLayout().length(); i++) {

        }
        floor.setLayout(sb.toString());
    }

}
