package com.nespresso.exercise.pisa;

/**
 * Created by ildiko.antal on 12/1/2016.
 */
public class Floor {
    private int wall;
    private int window;
    private String layout = "";

    public Floor(int wall) {
        this.wall = wall;
        for (int i = 0; i < wall; i++) {
            this.layout += "X";
        }
    }

    public Floor(int wall, int window) {
        this(wall);
        this.wall = wall - window;
        this.window = window;
        int windowPosition = (int) Math.ceil((wall - window) / 2);
        StringBuilder sb = new StringBuilder(this.layout);

        for (int i = 0; i < window; i++) {
            sb.replace(windowPosition + i, windowPosition + i + 1, "0");
        }


        // the margins needs to be walls
        if (sb.indexOf("0") == 0) {
            sb.replace(0, 1, "X");
            wall = wall + 1;
            window = window - 1;
        }
        // the margins needs to be walls
        if (sb.lastIndexOf("0") == layout.length()-1) {
            sb.replace(layout.length() - 1, layout.length(), "X");
            wall = wall + 1;
            window = window - 1;
        }

        this.layout = sb.toString();
    }

    void setLayout(String layout){
        this.layout = layout;
    }

    String getLayout(){
        return this.layout;
    }

    public int getFloorSize() {
        return this.window + this.wall;

    }

    public String printOut() {
        return this.layout;
    }
}
